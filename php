/*JEY HERB*/
               if (isset($data['product_herb'])) {
                       foreach ($data['product_herb'] as $product_image) {
                               $this->db->query("INSERT INTO " . DB_PREFIX . "herb_image SET product_id = '" . (int)$product_id . "', image = '" . $this->db->escape($product_image['image']) . "', sort_order = '" . (int)$product_image['sort_order'] . "'");
                                       $product_image_id = $this->db->getLastId();
                                       foreach ($product_image['descriptions'] as $language_id => $value) {
                                               if (!isset($value['alt_text']) || $value['alt_text'] == '') {
                                                       $value['alt_text'] = '';
                                               }
                                               if (!isset($value['title_text']) || $value['title_text'] == '') {
                                                       $value['title_text'] = '';
                                               }
                                               $this->db->query("INSERT INTO " . DB_PREFIX . "herb_image_description SET product_image_id = '" . (int)$product_image_id . "', language_id = '" . (int)$language_id . "', product_id = '" . (int)$product_id . "', alt_text = '" . $this->db->escape($value['alt_text']) . "', title_text = '" . $this->db->escape($value['title_text']) . "'");
                                       }
                       }
               }
